FROM docker.io/library/alpine:3.13

RUN apk update \
    && apk add \
        python3-dev \
        py3-psycopg2 \
        py3-setproctitle \
        py3-pygments \
        py3-sqlparse \
        py3-wheel \
        py3-pip \
    && pip install --no-cache-dir pgcli \
    && apk del python2-dev py3-pip

ADD pgcli.sh /

ENTRYPOINT ["/pgcli.sh"]
