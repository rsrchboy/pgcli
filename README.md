# pgcli

[pgcli](https://www.pgcli.com/) is a neat little tool.  With the right
entrypoint, it's fairly straight-forward to run this in a container, using the
same parameters supported by [docker.io/library/postgres](https://hub.docker.com/_/postgres).

| Variable            | default | comment                                             |
|---------------------|---------|-----------------------------------------------------|
| `POSTGRES_HOST`     | _none_  | HOST to use to connect to the postgres instance     |
| `POSTGRES_USER`     | _none_  | USER to use to connect to the postgres instance     |
| `POSTGRES_PASSWORD` | _none_  | PASSWORD to use to connect to the postgres instance |
| `POSTGRES_DB`       | _none_  | DB to use to connect to the postgres instance       |

If any of those variables are unset or empty, we try reading the value from a
file (e.g. a secrets file).

| Variable            | default                 |
|---------------------|-------------------------|
| `POSTGRES_HOST`     | `/run/secrets/host`     |
| `POSTGRES_USER`     | `/run/secrets/user`     |
| `POSTGRES_PASSWORD` | `/run/secrets/password` |
| `POSTGRES_DB`       | `/run/secrets/db`       |
