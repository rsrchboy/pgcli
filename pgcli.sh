#!/bin/sh

# set -x

url=$1

# if we're given a parameter, try to use it as a connection url
if [ "x$url" != "x" ]; then
    /usr/bin/pgcli "$url"
    exit
fi

# otherwise, connect using the same env. variables that
# docker.io/library/postgres uses.

# if ..._FILE is empty, try a (reasonably) standardish secrets location; if
# that fails let the error percolate upwards.
POSTGRES_HOST="${POSTGRES_HOST:-$(cat ${POSTGRES_HOST_FILE:-/run/secrets/host})}"
POSTGRES_USER="${POSTGRES_USER:-$(cat ${POSTGRES_USER_FILE:-/run/secrets/user})}"
POSTGRES_PASSWORD="${POSTGRES_PASSWORD:-$(cat ${POSTGRES_PASSWORD_FILE:-/run/secrets/password})}"
POSTGRES_DB="${POSTGRES_DB:-$(cat ${POSTGRES_DB_FILE:-/run/secrets/db})}"

err=""

if [ "x$POSTGRES_HOST" = "x" ] ; then
    echo 'HOST must be specified! (e.g. $POSTGRES_HOST, _FILE)' 1>&2
    err=1
fi

if [ "x$POSTGRES_USER" = "x" ] ; then
    echo 'USER must be specified! (e.g. $POSTGRES_USER, _FILE)' 1>&2
    err=1
fi

if [ "x$POSTGRES_PASSWORD" = "x" ] ; then
    echo 'PASSWORD must be specified! (e.g. $POSTGRES_PASSWORD, _FILE)' 1>&2
    err=1
fi

if [ "x$POSTGRES_DB" = "x" ] ; then
    echo 'DB must be specified! (e.g. $POSTGRES_DB, _FILE)' 1>&2
    err=1
fi

if [ "x$err" != "x" ] ; then
    exit 1
fi

/usr/bin/pgcli postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/${POSTGRES_DB:-postgres}
